package com.kangjusang.onestore.appa.ui.OptionalService;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.ui.OptionalServiceActivity;

/**
 * KT가 선택되었을 때 표시될 부가서비스 선택 화면
 */
public class OptionalServiceB extends Fragment {

    Button button, button2, button3, button4, button5;

    /**
     * 부가서비스 선택 시, 해당 부가서비스 상세 설명 페이지로 전환한다.
     */
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Activity activity = getActivity();
            if (activity instanceof OptionalServiceActivity) {
                ((OptionalServiceActivity) activity).switchToDetails();
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_optional_service_b, container, false);

        button = (Button)view.findViewById(R.id.button);
        button2 = (Button)view.findViewById(R.id.button2);
        button3 = (Button)view.findViewById(R.id.button3);
        button4 = (Button)view.findViewById(R.id.button4);
        button5 = (Button)view.findViewById(R.id.button5);
        button.setOnClickListener(onClickListener);
        button2.setOnClickListener(onClickListener);
        button3.setOnClickListener(onClickListener);
        button4.setOnClickListener(onClickListener);
        button5.setOnClickListener(onClickListener);

        return view;
    }
}
