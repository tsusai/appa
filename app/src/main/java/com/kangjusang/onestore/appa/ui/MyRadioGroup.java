package com.kangjusang.onestore.appa.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;

/**
 * 라디오 그룹 내에 선택된 라디오버튼을 식별하기 위해 key 값을 관리하는 기능을 추가한 클래스.
 * (라디오 그룹 내의 멤버 배치가 유동적인 경우,
 * 특정 순번의 라디오 버튼이 어떤 데이터에 매칭되어 있는지 별도로 관리할 필요가 있음)
 */
public class MyRadioGroup extends RadioGroup {

    private String selectedButtonKey = null;

    public MyRadioGroup(Context context) {
        super(context);
    }

    public MyRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSelectedButtonKey(String key) {
        selectedButtonKey = key;
    }

    public String getSelectedButtonKey() {
        return selectedButtonKey;
    }
}
