package com.kangjusang.onestore.appa.data;

import android.content.ContentValues;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.util.Date;

import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_FIRST_UPDATE_TIME;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_INSTALLER_PACKAGE_NAME;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_LAST_UPDATE_TIME;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_PACKAGE_NAME;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_SIZE_INSTALLED;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_SOURCE_DIR;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_VERSION_NAME;

/**
 *  기기에 설치된 앱의 정보를 가지고 있는 데이터 구조
 */
public class AppInstalled {

    private String packageName;
    private String installerPackageName;
    private String versionName;
    private long lastUpdateTime;
    private long firstInstallTime;
    private String sourceDir;
    private long sizeInstalled;

    /**
     *  기기에 설치된 앱을 제공한 앱마켓에 대한 열거형 데이터
     */
    public enum AppMarket {

        FACTORY_INSTALLED("null", "Factory installed"),
        ONE_STORE("com.skt.skaf.A000Z00040", "ONE store"),
        GOOGLE_PLAY_STORE("com.android.vending", "Google Play 스토어"),
        GALAXY_APPS("com.sec.android.app.samsungapps", "Galaxy Apps"),
        LOCAL("com.google.android.packageinstaller", "직접 설치"),
        UNKNOWN(null, "알 수 없음");

        private String mPackageName;
        private String mVisibleName;

        AppMarket(String pkgName, String visibleName) {
            this.mPackageName = pkgName;
            this.mVisibleName = visibleName;
        }

        public String getPackageName() {
            return mPackageName;
        }
        public String getVisibleName() {
            return mVisibleName;
        }

        /**
         *  정의된 앱마켓들 중 특정 패키지명에 해당하는 앱마켓을 반환한다.
         *
         * @param pkgName 앱마켓을 특정지을 패키지명
         * @return 패키지명에 해당한느 앱마켓이 존재하면 해당 앱마켓을, 그 밖의 경우 {@link #UNKNOWN} 타입의 앱마켓을 반환한다.
         */
        public static AppMarket getAppMarketByPackageName(String pkgName) {
            for (AppMarket appMarket : AppMarket.values()) {
                String packageName = appMarket.getPackageName();
                if (packageName != null && packageName.equals(pkgName)) {
                    return appMarket;
                }
            }
            return UNKNOWN;
        }

        /**
         *  정의된 앱마켓들 중 {@link #UNKNOWN} 타입을 제외한 나머지 앱마켓들의 패키지명 배열을 반환한다.
         *
         * @return 패키지명 배열
         */
        public static String[] getAppMarketPackageNamesExceptUnknown() {
            String[] packageNames = new String[AppMarket.values().length-1];
            int index = 0;
            for (AppMarket appMarket : AppMarket.values()) {
                if (appMarket == UNKNOWN)
                    continue;
                String pn = appMarket.getPackageName();
                packageNames[index++] = pn;
            }
            return packageNames;
        }
    }

    /**
     * 생성자
     *
     * @param pm
     * @param packageName
     */
    public AppInstalled(PackageManager pm, @NonNull String packageName) {

        this.packageName = packageName;

        String appMarketPackageName = pm.getInstallerPackageName(packageName);
        this.installerPackageName = appMarketPackageName;

        try {
            PackageInfo packageInfo = pm.getPackageInfo(packageName,0);
            String versionName = packageInfo.versionName;
            this.versionName = versionName;

            long lastUpdateTime = packageInfo.lastUpdateTime;
            this.lastUpdateTime = lastUpdateTime;

            long firstInstallTime = packageInfo.firstInstallTime;
            this.firstInstallTime = firstInstallTime;

            String sourceDir = packageInfo.applicationInfo.sourceDir;
            this.sourceDir = sourceDir;

            File file = new File(sourceDir);
            long size = file.length();
            this.sizeInstalled = size;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

//        printLog();
    }


    String getPackageName() {
        return packageName;
    }
    private String getAppMarketPackageName() {
        return installerPackageName;
    }
    private String getVersionName() {
        return versionName;
    }
    private long getLastUpdateTime() {
        return lastUpdateTime;
    }
    private long getFirstInstallTime() {
        return firstInstallTime;
    }
    private String getSourceDir() {
        return sourceDir;
    }
    private long getSizeInstalled() {
        return sizeInstalled;
    }

    /**
     * {@link android.database.sqlite.SQLiteDatabase} 클래스의 주요 메서드에 사용되는
     * {@link ContentValues}클래스의 인스턴스 값을 일괄적으로 채운다.
     *
     * @param contentValues
     */
    void autofillContentValues(ContentValues contentValues) {

        if (contentValues != null) {
            contentValues.put(KEY_PACKAGE_NAME, getPackageName());
            contentValues.put(KEY_INSTALLER_PACKAGE_NAME, getAppMarketPackageName());
            contentValues.put(KEY_VERSION_NAME, getVersionName());
            contentValues.put(KEY_LAST_UPDATE_TIME, getLastUpdateTime());
            contentValues.put(KEY_FIRST_UPDATE_TIME, getFirstInstallTime());
            contentValues.put(KEY_SOURCE_DIR, getSourceDir());
            contentValues.put(KEY_SIZE_INSTALLED, getSizeInstalled());
        }
    }

    /**
     * 디버깅 목적
     */
    public void printInfo() {
        Log.i("AppInstalled",
                packageName + "\t" + installerPackageName + "\t" + versionName + "\t"
                        + new Date(lastUpdateTime).toString()  + "\t" + new Date(firstInstallTime).toString() + "\t"
                        + sourceDir + "\t" +sizeInstalled);
    }

}
