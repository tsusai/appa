package com.kangjusang.onestore.appa.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.kangjusang.onestore.appa.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 노티피케이션을 띄우고 그 안의 내용을 갱신하는 역할을 하는 서비스
 */
public class NotificationIntentService extends IntentService {

    private static final String ACTION_SHOW_NOTIFICATION = "com.kangjusang.onestore.appa.action.SHOW_NOTIFICATION";

    private static final String EXTRA_MESSAGE = "com.kangjusang.onestore.appa.extra.MESSAGE";
    private static final String EXTRA_MESSAGE_DONE = "com.kangjusang.onestore.appa.extra.MESSAGE_DONE";
    private static final String EXTRA_END_AT = "com.kangjusang.onestore.appa.extra.END_AT";

    public NotificationIntentService() {
        super("NotificationIntentService");
    }

    /**
     * 노티피케이션 띄우는 액션을 인텐트에 담아 서비스를 구동시키는 헬퍼 메소드
     *
     * @param context
     * @param message
     * @param messageDone
     * @param endAt
     */
    public static void startActionShowNotification(Context context, String message, String messageDone, Long endAt) {

        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_SHOW_NOTIFICATION);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE_DONE, messageDone);
        intent.putExtra(EXTRA_END_AT, endAt);
        context.startService(intent);
    }

    /**
     * 서비스가 인텐트를 수신하였을 때 호출된다.
     *
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SHOW_NOTIFICATION.equals(action)) {
                String message = intent.getStringExtra(EXTRA_MESSAGE);
                String messageDone = intent.getStringExtra(EXTRA_MESSAGE_DONE);
                Long endAt = intent.getLongExtra(EXTRA_END_AT, System.currentTimeMillis());
                handleActionStartNotification(message, messageDone, endAt);
            }
        }
    }

    /**
     * 노티피케이션을 띄우고 지정된 시각에 완료메시지로 변경한다.
     *
     * @param message 지정된 시각 이전까지 출력할 메시지
     * @param messageDone 지정된 시각 이후부터 출력할 메시지
     * @param endAt 지정된 시각
     */
    private void handleActionStartNotification(final String message, final String messageDone, final Long endAt) {

        final Long now = System.currentTimeMillis();

        if (endAt - now < 0L)
            return;

        Context context = getApplicationContext();

        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        /**
         * {@link PendingIntent}가 지정되지 않은 경우 API Level 10 단말에서 크래시 발생하여, 할일이 지정되지 않은 PendingIntent 선언.
         */
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        String contentText = String.format(message, (endAt - now)/1000 + 1);

        final NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(contentText);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentText(contentText)
                .setContentTitle("부가서비스 가입 중")
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(pendingIntent)
                .setAutoCancel(false)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setStyle(bigTextStyle);

        notificationManager.notify(1, builder.build());

        final Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {

            Long leftFor = endAt - now;

            @Override
            public void run() {

                Long now = System.currentTimeMillis();
                if ( endAt - now > 0L ) {

                    /**
                     * 노티피케이션 메시지의 '남은 시간' 갱신의 불필요한 반복을 줄이기 위해,
                     * 초 단위의 시간값이 변경된 경우에만 notify를 호출한다.
                     */
                    if ((endAt - now)/1000 < leftFor /1000) {

                        leftFor = endAt - now;

                        String contentText = String.format(message, leftFor /1000 + 1);
                        bigTextStyle.bigText(contentText);
                        builder.setContentText(contentText);
                        builder.setStyle(bigTextStyle);
                        notificationManager.notify(1, builder.build());
                    }

                } else {
                    leftFor = endAt - now;

                    bigTextStyle.bigText(messageDone);
                    builder.setContentTitle("부가서비스 가입 완료");
                    builder.setContentText(messageDone);
                    builder.setOngoing(false);
                    builder.setAutoCancel(true);
                    builder.setStyle(bigTextStyle);
                    notificationManager.notify(1, builder.build());

                    timer.cancel();
                    timer.purge();

                }
            }
        };

        /**
         * 비교적 정확한 시각(초) 카운팅을 위해 0.1초 수준의 인터벌로 체크.
         */
        timer.scheduleAtFixedRate(timerTask, 0, 100);

    }

}
