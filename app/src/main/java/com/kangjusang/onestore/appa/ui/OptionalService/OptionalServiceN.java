package com.kangjusang.onestore.appa.ui.OptionalService;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kangjusang.onestore.appa.R;

/**
 * 통신사가 선택되지 않았을 때 표시되는 부가서비스 선택 화면
 */
public class OptionalServiceN extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_optional_service_n, container, false);
    }
}
