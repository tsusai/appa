package com.kangjusang.onestore.appa.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kangjusang.onestore.appa.data.AppInstalled;
import com.kangjusang.onestore.appa.data.AppMarketDBHelper;
import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.data.SharedPreferenceSelector;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 로딩 화면 클래스. 권한 확인 및 단말에 설치된 앱 정보 수집을 수행한다.
 */
public class SplashActivity extends Activity {

    private static final int REQUEST_CODE_PERMISSION_GRANT = 4541;

    ProgressBar progressBar;

    /**
     * 권한 동의 결과를 받는다.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode != REQUEST_CODE_PERMISSION_GRANT) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            scanAppsInstalled();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setMax(100);

        /**
         * Android 6.0 이상부터는 권한 체크 필수.
         */
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermission();
        } else {
            scanAppsInstalled();
        }

    }

    /**
     * 퍼미션 관련 처리
     */
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {

            // 동의되지 않음
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_ACCOUNTS)) {

            }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, REQUEST_CODE_PERMISSION_GRANT);
        } else {
            // 이미 동의되어 있음
            scanAppsInstalled();
        }
    }

    /**
     * 단말에 설치된 앱 정보를 스캔한다.
     */
    private void scanAppsInstalled() {

        final Long startAt = System.currentTimeMillis();
        final Long lasfFor = 3000L;

        final Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Long beenFor = System.currentTimeMillis() - startAt;
                if ( beenFor > lasfFor) {
                    progressBar.setProgress(100);
                    timer.cancel();
                    timer.purge();
                    goToAppMarketSelection();
                } else {
                    progressBar.setProgress((int)(beenFor*100/lasfFor));
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0,300L);

        // 앱 목록 받아오기
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> appList;
        AppInstalled[] appsInstalled;
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        appList = packageManager.queryIntentActivities(intent, 0);

        // 데이터베이스에 앱 별 정보 저장(추가 및 갱신)하기
        AppMarketDBHelper appMarketDBHelper = new AppMarketDBHelper(this);
        appsInstalled = new AppInstalled[appList.size()];
        long now = System.currentTimeMillis();
        for (int i = 0 ; i < appList.size(); i++) {
            ResolveInfo resolveInfo = appList.get(i);
            String packageName = resolveInfo.activityInfo.packageName;
            appsInstalled[i] = new AppInstalled(packageManager, packageName);
            appMarketDBHelper.addOrUpdateAppInstalled(appsInstalled[i], now);
        }

        // 설치되었다가 현재는 삭제된 앱들에 대해 데이터베이스에서 제거하기.
        appMarketDBHelper.deleteAppsRemoved(now);

    }

    /**
     * 앱마켓 선택 화면으로 진입한다.
     */
    private void goToAppMarketSelection() {

        SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(this, SharedPreferenceSelector.SELECT_MODE.REAL);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

        Intent intent = new Intent(this, AppMarketSelectionActivity.class);
        startActivity(intent);
        finish();
    }
}
