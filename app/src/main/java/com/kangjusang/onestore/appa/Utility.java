package com.kangjusang.onestore.appa;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;

/**
 * 잡다한 기능
 */
public class Utility {

    /**
     * 통신사 열거형 데이터
     */
    public enum CARRIER {

        SKT(0,"SKTelecom"),
        KT(1,"KT"),
        UPlus(2,"LG U+"),
        NotApplicable(3, "N/A");

        private int mId;
        private String mVisibleName;

        CARRIER(int id, String visibleName) {
            this.mId = id;
            this.mVisibleName = visibleName;
        }

        public int getId() {
            return mId;
        }
        public String getVisibleName() {
            return mVisibleName;
        }

        public static CARRIER getCarrierById(int id) {
            for (CARRIER carrier : CARRIER.values()) {
                if (carrier.mId == id)
                    return carrier;
            }
            return NotApplicable;
        }

        public static CARRIER getCarrierByVisibleName(String visibleName) {
            for (CARRIER carrier : CARRIER.values()) {
                if (carrier.mVisibleName.equals(visibleName))
                    return carrier;
            }
            return NotApplicable;
        }
    };

    /**
     * 이메일 형식이 맞는지 체크한다.
     *
     * @param emailAddress
     * @return
     */
    public static boolean isRightEmailFormat(String emailAddress) {
        if (!(emailAddress.contains("@") && emailAddress.contains(".")))
            return false;
        if (emailAddress.endsWith("."))
            return false;
        if (!(emailAddress.lastIndexOf(".") - emailAddress.lastIndexOf("@") > 1))
            return false;
        if (emailAddress.startsWith("@"))
            return false;
        return true;
    }

    /**
     * 단말이 제공하는 통신사 이름을 반환한다.
     *
     * @param context
     * @return
     */
    public static String getRealCarrierName(Context context) {
        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        return carrierName;
    }

    /**
     * UI 스레드에서 작업을 수행한다.
     *
     * @param r 수행할 작업
     */
    public static void runOnMainThread(Runnable r) {

        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            r.run();
        }
        else {
            new Handler(Looper.getMainLooper()).post(r);
        }
    }
}
