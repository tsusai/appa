package com.kangjusang.onestore.appa.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

/**
 * {@link AppInstalled}클래스 데이터를 저장하기 위한 데이터베이스
 */
public class AppMarketDBHelper extends SQLiteOpenHelper {

    private final static String DB_NAME = "APPS_INSTALLED";
    private final static String TABLE_NAME = "APPS_INSTALLED";

    public final static String KEY_ID = "_ID";
    public final static String KEY_PACKAGE_NAME = "PACKAGE_NAME";
    public final static String KEY_INSTALLER_PACKAGE_NAME = "INSTALLER_PACKAGE_NAME";
    public final static String KEY_VERSION_NAME = "VERSION_NAME";
    public final static String KEY_LAST_UPDATE_TIME = "LAST_UPDATE_TIME";
    public final static String KEY_FIRST_UPDATE_TIME = "FIRST_UPDATE_TIME";
    public final static String KEY_SOURCE_DIR = "SOURCE_DIR";
    public final static String KEY_SIZE_INSTALLED = "SIZE_INSTALLED";
    public final static String KEY_CONFIRM_TIME = "CONFIRM_TIME";

    /**
     * 생성자
     *
     * @param context
     */
    public AppMarketDBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        StringBuffer sb = new StringBuffer();
        sb.append(" CREATE TABLE " + TABLE_NAME + " ( ");
        sb.append(" _ID INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sb.append(" ").append(KEY_PACKAGE_NAME).append(" TEXT, ");
        sb.append(" ").append(KEY_INSTALLER_PACKAGE_NAME).append(" TEXT, ");
        sb.append(" ").append(KEY_VERSION_NAME).append(" TEXT, ");
        sb.append(" ").append(KEY_LAST_UPDATE_TIME).append(" INTEGER, ");
        sb.append(" ").append(KEY_FIRST_UPDATE_TIME).append(" INTEGER, ");
        sb.append(" ").append(KEY_SOURCE_DIR).append(" TEXT, ");
        sb.append(" ").append(KEY_SIZE_INSTALLED).append(" INTEGER, ");
        sb.append(" ").append(KEY_CONFIRM_TIME).append(" INTEGER ) ");

        db.execSQL(sb.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    /**
     * 데이터베이스에 {@link AppInstalled} 데이터 한 건을 추가하거나 갱신한다.
     *
     * @param appInstalled 추가 또는 갱신할 데이터
     * @param now 업데이트 여부를 결정하기 위해 사용하는 '현재 시각' 값
     */
    public void addOrUpdateAppInstalled(AppInstalled appInstalled, long now) {

        int _id = selectAppIdInstalled(appInstalled);

        if (_id < 0) {
            addAppInstalled(appInstalled, now);
        } else {
            updateAppInstalled(appInstalled, _id, now);
        }
    }

    /**
     * 단말에 설치된 특정앱에 해당하는 key 값을 데이터베이스에서 가져와 반환한다.
     * (신규 설치 앱인지의 여부를 판단하기 위해 사용)
     *
     * @param appInstalled
     * @return 해당하는 앱이 데이터베이스에 있는 경우 데이터베이스 상에서의 key 값을,
     * 그렇지 않은 경우 -1을 반환한다.
     */
    public int selectAppIdInstalled(AppInstalled appInstalled) {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[] { KEY_ID },KEY_PACKAGE_NAME + "=?",
                new String[] { appInstalled.getPackageName() }, null, null, null, null);
        if (cursor != null) {
            int count = cursor.getCount();
            if (count > 0) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(KEY_ID);
                return cursor.getInt(columnIndex);
            }
            return -1;
        }
        else {
            return -1;
        }
    }

    /**
     * 특정 앱마켓에 의해 설치된 앱의 목록을 데이터베이스에서 가져와 반환한다.
     *
     * @param appMarket
     * @return 데이터베이스에서 조회한 결과 집합에 대한 커서.
     * 결과의 갯수가 1개 이상인 경우 커서는 결과 집합의 맨 처음을 가리킨다.
     */
    public Cursor selectAppInstalledByAppMarket(AppInstalled.AppMarket appMarket) {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, null, KEY_INSTALLER_PACKAGE_NAME + "=?",
                new String[] { appMarket.getPackageName() }, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * 알 수 없는 - {@link com.kangjusang.onestore.appa.data.AppInstalled.AppMarket}에서 UNKNOWN으로 인식한 - 앱마켓에서 설치된 앱의 목록을
     * 데이터베이스에서 가져와 반환한다.
     *
     * @return 데이터베이스에서 조회한 결과 집합에 대한 커서.
     * 결과의 갯수가 1개 이상인 경우 커서는 결과 집합의 맨 처음을 가리킨다.
     */
    public Cursor selectAppInstalledByUnknownAppMarket() {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String[] packageNamesToExclude = AppInstalled.AppMarket.getAppMarketPackageNamesExceptUnknown();
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < packageNamesToExclude.length ; i++) {
            if (0 < i)
                sb.append(" AND ");
            sb.append("'").append(KEY_INSTALLER_PACKAGE_NAME).append("'!=?");
        }

        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[] { KEY_SIZE_INSTALLED, "_id", KEY_PACKAGE_NAME }, sb.toString(),
                packageNamesToExclude, null, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
            }
        }
        return cursor;
    }

    /**
     * 단말에서 삭제된 앱을 데이터베이스에서 제거한다.
     *
     * @param now 지금 시각
     * @return 데이터베이스에서 제거한 앱 갯수
     */
    public int deleteAppsRemoved(long now) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        int resultDeleteCount = sqLiteDatabase.delete(TABLE_NAME, KEY_CONFIRM_TIME + " < " + now, null);
        sqLiteDatabase.close();

        return resultDeleteCount;
    }

    /**
     * 데이터베이스에 앱 정보를 추가한다.
     *
     * @param appInstalled 추가할 앱 데이터
     * @param now 지금 시각
     */
    private void addAppInstalled(AppInstalled appInstalled, long now) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        appInstalled.autofillContentValues(contentValues);
        contentValues.put(KEY_CONFIRM_TIME, now);

        sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
        sqLiteDatabase.close();
    }

    /**
     * 데이터베이스에 앱 정보를 갱신한다.
     *
     * @param appInstalled 갱신할 앱 데이터
     * @param id 갱신할 앱 정보에 해당하는 데이터베이스 상의 key 값
     * @param now 지금 시각
     * @return 갱신 성공 여부
     */
    private boolean updateAppInstalled(AppInstalled appInstalled, int id, long now) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        appInstalled.autofillContentValues(contentValues);
        contentValues.put(KEY_CONFIRM_TIME, now);

        boolean resultUpdate = sqLiteDatabase.update(TABLE_NAME, contentValues, KEY_ID + "=" + id, null) > 0;
        sqLiteDatabase.close();

        return resultUpdate;
    }


}
