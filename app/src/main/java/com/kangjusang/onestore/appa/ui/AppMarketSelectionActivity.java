package com.kangjusang.onestore.appa.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.kangjusang.onestore.appa.Utility;
import com.kangjusang.onestore.appa.data.AppInstalled;
import com.kangjusang.onestore.appa.data.AppMarketDBHelper;
import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.data.SharedPreferenceSelector;

import static com.kangjusang.onestore.appa.data.AppInstalled.AppMarket.ONE_STORE;
import static com.kangjusang.onestore.appa.data.AppMarketDBHelper.KEY_SIZE_INSTALLED;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_EMAIL;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_APP_MARKET;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_CARRIER;

public class AppMarketSelectionActivity extends AppCompatActivity {

    Activity activity = null;

    MyRadioGroup rgAppMarket;
    EditText etEmailAddress;
    Button btnOK;

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferenceSelector.checkTestData(this);

        SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(this, SharedPreferenceSelector.SELECT_MODE.AUTO);
        String emailAddress = sharedPreferences.getString(SP_KEY_EMAIL,"");
        etEmailAddress.setText(emailAddress);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        setContentView(R.layout.activity_main);

        getDeviceGoogleAccount();

        initUI();
    }

    private void initUI() {

        rgAppMarket = (MyRadioGroup)findViewById(R.id.rgAppMarket);

        SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(this, SharedPreferenceSelector.SELECT_MODE.REAL);
        AppInstalled.AppMarket prevSelectedAppMarket = AppInstalled.AppMarket.getAppMarketByPackageName(sharedPreferences.getString(SP_KEY_APP_MARKET, ONE_STORE.getPackageName()));

        AppMarketDBHelper appMarketDBHelper = new AppMarketDBHelper(this);

        for (final AppInstalled.AppMarket appMarket : AppInstalled.AppMarket.values()) {

            Cursor cursor = null;

            switch (appMarket) {

                case FACTORY_INSTALLED:
                    continue;

                case ONE_STORE:
                case GOOGLE_PLAY_STORE:
                case GALAXY_APPS:
                    cursor = appMarketDBHelper.selectAppInstalledByAppMarket(appMarket);
                    break;

                case LOCAL:
//                    cursor = appMarketDBHelper.selectAppInstalledByAppMarket(appMarket);
                    continue;

                case UNKNOWN:
//                    cursor = appMarketDBHelper.selectAppInstalledByUnknownInstaller();
                    continue;
            }

            int appInstalledCount = 0;
            long appInstalledSizeTotal = 0;

            if (cursor != null && cursor.getCount() > 0) {

                do {
                    appInstalledCount++;
                    appInstalledSizeTotal += Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_SIZE_INSTALLED)));

                } while (cursor.moveToNext());
            }

            String appInstalledDescription = appMarket.getVisibleName() + ", " + appInstalledCount + "개, " + appInstalledSizeTotal / 1024 + "KB";
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(appInstalledDescription);
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rgAppMarket.setSelectedButtonKey(appMarket.getPackageName());
                }
            });

            rgAppMarket.addView(radioButton);

            if (appMarket == prevSelectedAppMarket) {
                radioButton.setChecked(true);
                rgAppMarket.setSelectedButtonKey(prevSelectedAppMarket.getPackageName());
            }

        }


        etEmailAddress = (EditText)findViewById(R.id.etEmailAddress);



        btnOK = (Button)findViewById(R.id.btnMainActivityOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailAddress = etEmailAddress.getText().toString();
                if (Utility.isRightEmailFormat(emailAddress)) {
                    // save
                    SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(activity, SharedPreferenceSelector.SELECT_MODE.REAL);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(SP_KEY_APP_MARKET, rgAppMarket.getSelectedButtonKey());
                    editor.putString(SP_KEY_EMAIL, emailAddress);
                    editor.putInt(SP_KEY_CARRIER, Utility.CARRIER.getCarrierByVisibleName(Utility.getRealCarrierName(activity)).getId());
                    editor.commit();

                    moveToOptionalServiceSelection();
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    AlertDialog alertDialog = builder.setTitle("Email Address").setMessage("Email 주소를 입력하세요").setPositiveButton("확인",null).setCancelable(true).create();
                    alertDialog.show();
                }
            }
        });

    }

    /**
     * 부가서비스 선택 화면으로 전환한다.
     */
    private void moveToOptionalServiceSelection() {

        Intent intent = new Intent(this, OptionalServiceActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * 단말에 등록된 구글 계정을 가져와 저장한다.
     * 복수개의 구글 계정이 등록되어 있는 경우, 맨 첫 번째 구글 계정을 저장한다.
     */
    private void getDeviceGoogleAccount() {

        Account[] accounts = AccountManager.get(getApplicationContext()).getAccounts();
        for(Account account : accounts) {
            if(account.type.equals("com.google")){
                SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(this, SharedPreferenceSelector.SELECT_MODE.REAL);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(SP_KEY_EMAIL,account.name);
                editor.commit();
                return;
            }
        }
    }
}
