package com.kangjusang.onestore.appa.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.data.SharedPreferenceSelector;
import com.kangjusang.onestore.appa.ui.OptionalService.OptionalServiceMain;
import com.kangjusang.onestore.appa.ui.OptionalService.OptionalServiceDetails;

/**
 * 부가서비스 선택 및 가입에 관련된 화면.
 * Main과 Details로 나뉘며, Main은 선택화면, Details는 선택한 부가서비스 세부 정보를 보여주는 화면이다.
 */
public class OptionalServiceActivity extends AppCompatActivity {

    boolean isShowingDetails = false;

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferenceSelector.checkTestData(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_optional_service);
        switchToMain();
    }

    @Override
    public void onBackPressed() {
        if (isShowingDetails)
            switchToMain();
        else
            goToAppMarketSelection();
    }

    public void switchToDetails() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentBody, new OptionalServiceDetails(),"OptionalServiceDetails");
        fragmentTransaction.commit();
        isShowingDetails = true;
    }

    public void switchToMain() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentBody, new OptionalServiceMain(), "OptionalServiceMain");
        fragmentTransaction.commit();
        isShowingDetails = false;
    }

    /**
     * 앱마켓 선택 화면으로 되돌아간다.
     */
    public void goToAppMarketSelection() {

        Intent intent = new Intent(this, AppMarketSelectionActivity.class);
        startActivity(intent);
        finish();
    }
}
