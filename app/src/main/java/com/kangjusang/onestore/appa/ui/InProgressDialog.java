package com.kangjusang.onestore.appa.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.Utility;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 어떤 작업이 수행중임을 나타내기 위한 다이얼로그 클래스
 */
public class InProgressDialog extends Dialog {

    private TextView tvMessage;

    private InProgressDialogListener listener;

    private Long inProcessStartAt;
    private Long inProgressShouldEndAt;

    private Long timeLeft = 0L;
    private Long inProgressFor;

    private String messageInProgress;
    private String messageDone;

    /**
     * 다이얼로그 상의 유저 액션에 대한 콜백을 받기 위한 인터페이스
     */
    public interface InProgressDialogListener {
        void onCloseButton(String messageInProgress, String messageDone, Long inProgressShouldEndAt);
        void onCancelButton();
    }

    public InProgressDialog(@NonNull Context context, Long inProgressFor, InProgressDialogListener listener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.inProgressFor = inProgressFor;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.custom_dialog);
        setCancelable(false);

        initUi();
    }

    private void initUi() {

        inProcessStartAt = System.currentTimeMillis();
        inProgressShouldEndAt = inProcessStartAt + inProgressFor;

        tvMessage = (TextView)findViewById(R.id.tvMessage);
        Button btnClose = (Button) findViewById(R.id.btnCustomDialogClose);
        Button btnCancel = (Button) findViewById(R.id.btnCustomDialogCancel);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCloseButton(messageInProgress, messageDone, inProgressShouldEndAt);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCancelButton();
                dismiss();
            }
        });

        final Timer timer = new Timer();
        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Long now = System.currentTimeMillis();
                timeLeft = inProgressShouldEndAt - now;

                if (timeLeft < 0) {
                    timer.cancel();
                    timer.purge();
                    Utility.runOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            tvMessage.setText(messageDone);
                        }
                    });

                } else {
                    final String string = String.format(messageInProgress, timeLeft/1000 + 1);
                    Utility.runOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            tvMessage.setText(string);
                        }
                    });
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 100);
    }

    /**
     * 다이얼로그 위에 출력할 메시지를 입력받는다.
     *
     * @param inProgress 작업이 진행중일 때 출력할 메시지
     * @param done 작업이 완료되었을 때 출력할 메시지
     */
    public void setMessge(String inProgress, String done) {
        this.messageInProgress = inProgress;
        this.messageDone = done;
    }

}
