package com.kangjusang.onestore.appa.ui.OptionalService;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.Utility;
import com.kangjusang.onestore.appa.data.AppInstalled;
import com.kangjusang.onestore.appa.data.SharedPreferenceSelector;
import com.kangjusang.onestore.appa.ui.AppMarketSelectionActivity;

import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_EMAIL;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_APP_MARKET;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_CARRIER;

/**
 * 부가서비스 가입 화면의 상단에 표시되는 내용
 */
public class OptionalServiceHeader extends Fragment {

    AppCompatImageView imageViewSettings;
    TextView tvCarrierName, tvInstallerName, tvEmailAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_header, container, false);

        initUi(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // 선호 앱마켓은 REAL 에서 받아오기
        SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(getContext(), SharedPreferenceSelector.SELECT_MODE.REAL);
        String appMarket = sharedPreferences.getString(SP_KEY_APP_MARKET, null);
        tvInstallerName.setText(AppInstalled.AppMarket.getAppMarketByPackageName(appMarket).getVisibleName());

        // 나머지 정보는 받아올 위치는 AUTO 결정
        sharedPreferences = SharedPreferenceSelector.getSharedPreference(getContext(), SharedPreferenceSelector.SELECT_MODE.AUTO);
        String emailAddress = sharedPreferences.getString(SP_KEY_EMAIL, null);
        tvEmailAddress.setText(emailAddress);
        int carrierIndex = sharedPreferences.getInt(SP_KEY_CARRIER, Utility.CARRIER.SKT.getId());
        Utility.CARRIER carrier = Utility.CARRIER.getCarrierById(carrierIndex);
        tvCarrierName.setText(carrier.getVisibleName());
    }

    private void initUi(View view) {
        imageViewSettings = (AppCompatImageView)view.findViewById(R.id.ivSettings);
        imageViewSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBackToAppMarketSelection();
            }
        });
        tvCarrierName = (TextView)view.findViewById(R.id.tvCarrierName);
        tvInstallerName = (TextView)view.findViewById(R.id.tvInstallerName);
        tvEmailAddress = (TextView)view.findViewById(R.id.tvEmailAddress);
    }

    /**
     * 앱마켓 선택 화면으로 되돌아간다.
     */
    private void goBackToAppMarketSelection() {
        FragmentActivity parentActivity = getActivity();
        Intent intent = new Intent(parentActivity, AppMarketSelectionActivity.class);
        parentActivity.startActivity(intent);
        parentActivity.finish();
    }
}
