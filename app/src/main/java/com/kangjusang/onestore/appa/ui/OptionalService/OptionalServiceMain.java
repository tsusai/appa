package com.kangjusang.onestore.appa.ui.OptionalService;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.Utility;
import com.kangjusang.onestore.appa.data.SharedPreferenceSelector;

import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_CARRIER;

/**
 * 부가서비스 선택 화면의 메인 영역
 */
public class OptionalServiceMain extends Fragment {

    @Override
    public void onResume() {
        super.onResume();

        /**
         * 통신사에 따라 UI 구성이 달라짐
         */
        SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(getContext(), SharedPreferenceSelector.SELECT_MODE.AUTO);
        int carrierIndex = sharedPreferences.getInt(SP_KEY_CARRIER, Utility.CARRIER.SKT.getId());
        Utility.CARRIER carrier = Utility.CARRIER.getCarrierById(carrierIndex);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (carrier) {
            case SKT:
                fragmentTransaction.replace(R.id.fragment_optional_service, new OptionalServiceA());
                break;
            case KT:
                fragmentTransaction.replace(R.id.fragment_optional_service, new OptionalServiceB());
                break;
            case UPlus:
                fragmentTransaction.replace(R.id.fragment_optional_service, new OptionalServiceC());
                break;
            case NotApplicable:
                fragmentTransaction.replace(R.id.fragment_optional_service, new OptionalServiceN());
                break;
        }
        fragmentTransaction.commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_optional_service, container, false);
    }
}
