package com.kangjusang.onestore.appa.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;

/**
 * 테스트 모드 여부에 따라 사용할 {@link SharedPreferences} 데이터를 골라주는 클래스
 */
public class SharedPreferenceSelector {

    /**
     * {@link SharedPreferences}에서 데이터 조회 시 사용하는 키 값
     */
    public final static String SP_KEY_APP_MARKET = "app_market";
    public final static String SP_KEY_EMAIL = "email_address";
    public final static String SP_KEY_CARRIER = "carrier";
    public final static String SP_KEY_IS_TEST_MODE = "is_test_mode";

    /**
     * 사용할 {@link SharedPreferences} 별 명칭
     */
    public final static String SP_NAME_REAL = "shared_preference";
    public final static String SP_NAME_TEST = "shared_preference_test";

    /**
     * 자동 구분, 수동 선택(실제 값), 수동 선택(테스트 값)
     */
    public enum SELECT_MODE {
        AUTO,REAL,TEST;
    }

    /**
     * 조건에 맞는 {@link SharedPreferences} 인스턴스를 반환한다.
     *
     * @param context
     * @param selectMode
     * @return {@link SharedPreferences} 인스턴스
     */
    public static SharedPreferences getSharedPreference(Context context, SELECT_MODE selectMode) {

        SharedPreferences sharedPreferencesTest = context.getSharedPreferences("shared_preference_test", Context.MODE_PRIVATE);

        switch (selectMode) {
            case TEST:
                return sharedPreferencesTest;
            case AUTO:
                if (sharedPreferencesTest != null && sharedPreferencesTest.getBoolean(SP_KEY_IS_TEST_MODE, false)) {
                    return sharedPreferencesTest;
                }
            case REAL:
            default:
                return context.getSharedPreferences("shared_preference", Context.MODE_PRIVATE);
        }
    }

    /**
     * AppB의 데이터가 변경되었는지 감지하고 최신의 값으로 갱신한다. 앱이 Resume 되었을 때 호출되어야 한다.
     *
     * @param context
     */
    public static void checkTestData(Context context) {
        SharedPreferences sharedPreferencesTest = context.getSharedPreferences("shared_preference_test", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesTest.edit();

        Cursor cursor = context.getContentResolver().query(Uri.parse("content://com.kangjusang.onestore.appb.TestDataProvider/users"),null,null,null,null);
        if (cursor != null && cursor.moveToFirst()) {
            int carrier = cursor.getInt(cursor.getColumnIndex("carrier"));
            String emailAddress = cursor.getString(cursor.getColumnIndex("gmail_address"));
            editor.putBoolean(SP_KEY_IS_TEST_MODE,true);
            editor.putInt(SP_KEY_CARRIER,carrier);
            editor.putString(SP_KEY_EMAIL,emailAddress);
        } else {
            editor.putBoolean(SP_KEY_IS_TEST_MODE,false);
        }
        editor.commit();
    }
}
