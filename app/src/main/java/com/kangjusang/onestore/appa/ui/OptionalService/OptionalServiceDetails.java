package com.kangjusang.onestore.appa.ui.OptionalService;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kangjusang.onestore.appa.R;
import com.kangjusang.onestore.appa.Utility;
import com.kangjusang.onestore.appa.data.SharedPreferenceSelector;
import com.kangjusang.onestore.appa.service.NotificationIntentService;
import com.kangjusang.onestore.appa.ui.InProgressDialog;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_EMAIL;
import static com.kangjusang.onestore.appa.data.SharedPreferenceSelector.SP_KEY_CARRIER;

/**
 * 선택된 부가서비스에 대한 상세 설명 페이지
 */
public class OptionalServiceDetails extends Fragment {

    TextView tvOptionalServiceDetails;
    Button btnSKT, btnKT, btnUPlus;

    /**
     * 가입하기(=확인) 버튼을 눌렀을 때 수행할 동작.
     */
    View.OnClickListener onClickListener = new View.OnClickListener() {

        /**
         * '가입중' 다이얼로그의 닫기 or 취소 버튼을 눌렀을 때 수행할 동작
         */
        InProgressDialog.InProgressDialogListener inProgressDialogListener = new InProgressDialog.InProgressDialogListener() {

            @Override
            public void onCloseButton(String messageInProgress, String messageDone, Long inProgressShouldEndAt) {
                Long timeLeft = inProgressShouldEndAt - System.currentTimeMillis();
                if (timeLeft > 0) {
                    NotificationIntentService.startActionShowNotification(getActivity().getApplicationContext(), messageInProgress, messageDone, inProgressShouldEndAt);
                }

                Activity activity = getActivity();
                if (activity != null)
                    activity.finish();
            }

            @Override
            public void onCancelButton() {

            }
        };

        @Override
        public void onClick(View v) {

            String messageInProgress = "부가서비스 가입중입니다.\n남은 시간 : %d초";
            String messageDone = "입력한 email(%s)로 주문내역서가 발송되었습니다.";
            SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(getContext(), SharedPreferenceSelector.SELECT_MODE.AUTO);
            String emailAddress = sharedPreferences.getString(SP_KEY_EMAIL, "");

            InProgressDialog inProgressDialog = new InProgressDialog(getActivity(), 15000L, inProgressDialogListener);
            inProgressDialog.setMessge(messageInProgress, String.format(messageDone, emailAddress));
            inProgressDialog.show();
        }
    };


    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = SharedPreferenceSelector.getSharedPreference(getContext(), SharedPreferenceSelector.SELECT_MODE.AUTO);
        int carrierIndex = sharedPreferences.getInt(SP_KEY_CARRIER, 0);
        Utility.CARRIER carrier = Utility.CARRIER.getCarrierById(carrierIndex);

        switch (carrier) {
            case SKT:
                tvOptionalServiceDetails.setText("부가서비스 정보 표시 화면입니다.");
                btnSKT.setVisibility(VISIBLE);
                btnKT.setVisibility(GONE);
                btnUPlus.setVisibility(GONE);
                break;
            case KT:
                tvOptionalServiceDetails.setText("부가서비스 정보 표시 화면입니다.");
                btnSKT.setVisibility(GONE);
                btnKT.setVisibility(VISIBLE);
                btnUPlus.setVisibility(GONE);
                break;
            case UPlus:
                tvOptionalServiceDetails.setText("부가서비스 정보 표시 화면입니다.");
                btnSKT.setVisibility(View.GONE);
                btnKT.setVisibility(GONE);
                btnUPlus.setVisibility(VISIBLE);
                break;
            case NotApplicable:
                tvOptionalServiceDetails.setText("통신사를 확인할 수 없습니다.");
                btnSKT.setVisibility(View.GONE);
                btnKT.setVisibility(GONE);
                btnUPlus.setVisibility(GONE);
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_optinal_service_details, container, false);

        tvOptionalServiceDetails = (TextView)view.findViewById(R.id.tvOptionalServiceDetails);
        btnSKT = (Button)view.findViewById(R.id.btnSKT);
        btnKT = (Button)view.findViewById(R.id.btnKT);
        btnUPlus = (Button)view.findViewById(R.id.btnUplus);

        btnSKT.setOnClickListener(onClickListener);
        btnKT.setOnClickListener(onClickListener);
        btnUPlus.setOnClickListener(onClickListener);

        return view;
    }
}
